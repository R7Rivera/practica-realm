package facci.pm.realmdb;

public enum ProductoStatus {

    Open("Open"),
    InProgress("In Progress"),
    Complete("Complete");
    String displayName;
    ProductoStatus(String displayName) {
        this.displayName = displayName;
    }

}
