package facci.pm.realmdb;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Producto extends RealmObject {
    @Required
    private String nombre;
    @Required
    private String status = TaskStatus.Open.name();
    @Required
    private String descripcion;
    @Required
    private String precio;
    @Required
    private String stock;
    @Required
    private String categoria;

    public Producto(String _nombre, String _descripcion, String _precio, String _stock, String _categoria) {
        this.nombre = _nombre;
        this.descripcion = _descripcion;
        this.precio = _precio;
        this.stock = _stock;
        this.categoria = _categoria;
    }

    public Producto() {

    }

    public void setStatus(ProductoStatus status) { this.status = status.name(); }
    public String getStatus() { return this.status; }

    //Nombre
    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    //Descripción
    public String getDescripcion() {return descripcion;}
    public void setDescripcion(String descripcion) {this.descripcion = descripcion;}

    //Precio
    public String getPrecio() {return precio;}
    public void setPrecio(String precio) {this.precio = precio;}

    //Stock
    public String getStock() {return stock;}
    public void setStock(String stock) {this.stock = stock;}

    //Categoría
    public String getCategoria() {return categoria;}
    public void setCategoria(String categoria) {this.categoria = categoria;}
}
