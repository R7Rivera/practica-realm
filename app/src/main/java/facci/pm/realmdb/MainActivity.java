package facci.pm.realmdb;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    Realm uiThreadRealm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Realm.init(this); // context, usually an Activity or Application

        //Open a Realm
        String realmName = "proyectorealm";
        RealmConfiguration config = new RealmConfiguration.Builder().name(realmName).build();
        uiThreadRealm = Realm.getInstance(config);

        addChangeListenerToRealm(uiThreadRealm);

        FutureTask<String> Producto = new FutureTask(new BackgroundQuickStart(), "test");
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.execute(Producto);
    }

    private void addChangeListenerToRealm(Realm realm) {

        // all Tasks in the realm
        RealmResults<Producto> Productos = uiThreadRealm.where(Producto.class).findAllAsync();
        Productos.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<Producto>>() {
            @Override
            public void onChange(RealmResults<Producto> collection, OrderedCollectionChangeSet changeSet) {
                // process deletions in reverse order if maintaining parallel data structures so indices don't change as you iterate
                OrderedCollectionChangeSet.Range[] deletions = changeSet.getDeletionRanges();
               for (OrderedCollectionChangeSet.Range range : deletions) {
                    Log.e("QUICKSTART", "Deleted range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
                }

                OrderedCollectionChangeSet.Range[] insertions = changeSet.getInsertionRanges();
                for (OrderedCollectionChangeSet.Range range : insertions) {
                    Log.e("QUICKSTART", "Inserted range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
                }

                OrderedCollectionChangeSet.Range[] modifications = changeSet.getChangeRanges();
                for (OrderedCollectionChangeSet.Range range : modifications) {
                    Log.e("QUICKSTART", "Updated range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // the ui thread realm uses asynchronous transactions, so we can only safely close the realm
        // when the activity ends and we can safely assume that those transactions have completed
        uiThreadRealm.close();
    }

    public static class BackgroundQuickStart implements Runnable {
        @Override
        public void run() {
            String realmName = "proyectorealm";
            RealmConfiguration config = new RealmConfiguration.Builder().name(realmName).build();
            Realm backgroundThreadRealm = Realm.getInstance(config);

            //Añadir
            //To create a new Task, instantiate an instance of the Task class and add it to the realm in a write block
            Producto producto1 = new Producto("Avena", "Instantanea", "$ 2.50 c/u", "100","Abarrotes");
            Producto producto2 = new Producto("Azucar", "Azucar blanca", "$ 1.50 c/u", "200","Abarrotes");
            Producto producto3 = new Producto("Aceite", "Aceite comestible", "$ 3.50 c/u", "140","Abarrotes");
            Producto producto4 = new Producto("Aceitunas", "Aceitunas enlatadas", "$ 2.50 c/u", "50","Productos enlatados");
            Producto producto5 = new Producto("Champiñones enteros/rebanados", " champiñones enlatadas", "$ 1.99 c/u", "270","Productos enlatados");
            Producto producto6 = new Producto("Frijoles", "Frijoles enlatados", "$ 3.60 c/u", "260","Productos enlatados");
            Producto producto7 = new Producto("leche", "lecha descremada", "$ 1.00 c/u", "460","Lacteos");
            Producto producto8 = new Producto("Queso", "queso mozarella", "$ 4.00 c/u", "600","Lacteos");
            Producto producto9 = new Producto("Yoghurt", "yoghurt natural", "$ 2.30 c/u", "480","Lacteos");
            Producto producto10 = new Producto("Caramelos", "dulce masticable", "$ 1.30 c/u", "350","Confiteria/dulceria");
            Producto producto11 = new Producto("Chocolate", "Chocolate", "$ 3.30 c/u", "150","Confiteria/dulceria");
            Producto producto12 = new Producto("Paletas de dulce", "Paletas de dulce", "$ 1.30 c/u", "199","Confiteria/dulceria");
            Producto producto13 = new Producto("Pan dulce", "Pan dulce de molde", "$ 2.30 c/u", "70","Harina/Pan");
            Producto producto14 = new Producto("Pan molido", "Pan molido", "$ 1.30 c/u", "60","Harina/Pan");
            Producto producto15 = new Producto("Pan tostado", "Pan tostado", "$ 2.30 c/u", "60","Harina/Pan");
            Producto producto16 = new Producto("Aguacates", "Aguacates medianos", "$ 1.30 c/u", "160","Frutas/verduras");
            Producto producto17 = new Producto("Cebollas", "cebollas colorada pequeña", "$ 1.20 c/u", "120","Frutas/verduras");
            Producto producto18 = new Producto("Limones", "Limones grandes", "$ 0.50 c/u", "70","Frutas/verduras");

            backgroundThreadRealm.executeTransaction (transactionRealm -> {
                transactionRealm.insert(producto1);
                transactionRealm.insert(producto2);
                transactionRealm.insert(producto3);
                transactionRealm.insert(producto4);
                transactionRealm.insert(producto5);
                transactionRealm.insert(producto6);
                transactionRealm.insert(producto7);
                transactionRealm.insert(producto8);
                transactionRealm.insert(producto9);
                transactionRealm.insert(producto10);
                transactionRealm.insert(producto11);
                transactionRealm.insert(producto12);
                transactionRealm.insert(producto13);
                transactionRealm.insert(producto14);
                transactionRealm.insert(producto15);
                transactionRealm.insert(producto16);
                transactionRealm.insert(producto17);
                transactionRealm.insert(producto18);

            });

            //Método Modificar
            //You can retrieve a live collection of all items in the realm
            // all Tasks in the realm
            RealmResults<Producto> Productos = backgroundThreadRealm.where(Producto.class).findAll();

            //To MODIFY a task, update its properties in a write transaction block
            Producto otherProducto = Productos.get(0);
//            // all modifications to a realm must happen inside of a write block
            backgroundThreadRealm.executeTransaction( transactionRealm -> {
                Producto innerOtherProducto = transactionRealm.where(Producto.class).equalTo("name", otherProducto.getNombre()).findFirst();
                innerOtherProducto.setStatus(ProductoStatus.Open);
            });


            //Método Eliminar
            //you can DELETE a task by calling the deleteFromRealm() method in a write transaction block:
            Producto yetAnotherProducto = Productos.get(0);
            String yetAnotherProductoNombre = yetAnotherProducto.getNombre();
            String yetAnotherProductoDescripcion = yetAnotherProducto.getDescripcion();
            String yetAnotherProductoPrecio = yetAnotherProducto.getPrecio();
            String yetAnotherProductoStock= yetAnotherProducto.getStock();
            String yetAnotherProductoCategoria= yetAnotherProducto.getCategoria();
//            // all modifications to a realm must happen inside of a write block
            backgroundThreadRealm.executeTransaction( transactionRealm -> {
                Producto innerYetAnotherProducto = transactionRealm.where(Producto.class)
                        .equalTo("_id", yetAnotherProductoNombre)
                        .equalTo("_id", yetAnotherProductoDescripcion)
                        .equalTo("_id", yetAnotherProductoPrecio)
                        .equalTo("_id", yetAnotherProductoStock)
                        .equalTo("_id", yetAnotherProductoCategoria)
                        .findFirst();
                innerYetAnotherProducto.deleteFromRealm();
            });
            for (Producto producto: Productos
            ) {
                Log.e("Task", "Name: " + producto.getNombre()
                        + " Status: " + producto.getStatus()
                        + " Descripcion: " + producto.getDescripcion()
                        + " Precio: " + producto.getPrecio()
                        + " Stock: " + producto.getStock()
                        + " Categoria: " + producto.getCategoria()
                );
            }


            // you can also filter a collection
            RealmResults<Producto> ProductosThatBeginWithN = Productos.where().beginsWith("nombre", "H").findAll();
            RealmResults<Producto> openProductos = Productos.where().equalTo("status", ProductoStatus.Open.name()).findAll();



            // because this background thread uses synchronous realm transactions, at this point all
            // transactions have completed and we can safely close the realm
            backgroundThreadRealm.close();
        }
    }
}